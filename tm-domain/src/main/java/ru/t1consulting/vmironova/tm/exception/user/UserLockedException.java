package ru.t1consulting.vmironova.tm.exception.user;

public final class UserLockedException extends AbstractUserException {

    public UserLockedException() {
        super("Error! User is locked, contact your administrator.");
    }

}
