package ru.t1consulting.vmironova.tm.api.service;

public interface IDomainService {

    void dataBackupLoad();

    void dataBackupSave();

    void dataBase64Load();

    void dataBase64Save();

    void dataBinaryLoad();

    void dataBinarySave();

    void dataJsonLoadFasterXml();

    void dataJsonLoadJaxB();

    void dataJsonSaveFasterXml();

    void dataJsonSaveJaxB();

    void dataXmlLoadFasterXml();

    void dataXmlLoadJaxB();

    void dataXmlSaveFasterXml();

    void dataXmlSaveJaxB();

    void dataYamlLoadFasterXml();

    void dataYamlSaveFasterXml();

}
