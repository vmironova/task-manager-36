package ru.t1consulting.vmironova.tm.api.endpoint;

import ru.t1consulting.vmironova.tm.dto.request.AbstractRequest;
import ru.t1consulting.vmironova.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
